﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class AnunciosContext : DbContext
    {
        public AnunciosContext() : base("name=AnunciosContext")
        {
        }

        public AnunciosContext(string connString)
            : base(connString)
        {
        }

        public System.Data.Entity.DbSet<Ad> Ads { get; set; }
    
    
    }
}
